(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [xs] (list? xs))
        g (fn [x] (map-entry? x))
        z (comp f g)]
    (z '(-1 2 -2 3 -3 4 -4))))

(defn función-comp-2
  []
  (let [f (fn [xs] (+ 15 xs))
        g (fn [x] (- 3 x))
        z (comp f g)]
    (z 5)))

(defn función-comp-3
  []
  (let [f (fn [xs] (inc xs))
        g (fn [x] (- 11 x))
        z (comp f g)]
    (z 1)))


(defn función-comp-4
  []
  (let [f (fn [xs] (into [] (remove pos? xs)))
        g (fn [x] (reverse x))
        h (fn [x] (map inc x))
        z (comp f g h)]
    (z [-1 2 -2 3 -3 4 -4])))

(defn función-comp-5
  []
  (let [f (fn [xs] (filter pos? xs))
        g (fn [x] (filter even? x))
        h (fn [x] (map inc x))
        z (comp f g h)]
    (z #{-1 2 -2 3 -3 4 -4})))


(defn función-comp-6
  []
  (let [f (fn [xs] (keyword xs))
        g (fn [x] (str x))
        z (comp f g)]
    (z [10 20])))


(defn función-comp-7
  []
  (let [f (fn [xs] (second xs))
        g (fn [x] (reverse x))
        z (comp f g)]
    (z '(true 10 20 "hello"))))


(defn función-comp-8
  []
  (let [f (fn [xs] (not xs))
        g (fn [x] (zero? x))
        z (comp f g)]
    (z 10)))


(defn función-comp-9
  []
  (let [f (fn [xs] (not xs))
        g (fn [x] (zero? x))
        z (comp f g)]
    (z 1000)))


(defn función-comp-10
  []
  (let [f (fn [xs] (str xs))
        g (fn [x] (double x))
        z (comp f g)]
    (z (+ 1 2 3))))

(defn función-comp-11
  []
  (let [f (fn [xs] (char? xs))
        g (fn [x] (first x))
        z (comp f g)]
    (z "Israel")))


(defn función-comp-12
  []
  (let [f (fn [xs] (ident? xs))
        g (fn [x] (empty x))
        z (comp f g)]
    (z [\a \b \c \d])))

(defn función-comp-13
  []
  (let [f (fn [xs] (indexed? xs))
        g (fn [x] (list x))
        z (comp f g)]
    (z {true false 1 2 \a \b})))

(defn función-comp-14
  []
  (let [f (fn [xs] (int? xs))
        g (fn [x] (first x))
        z (comp f g)]
    (z [1 2 3 4])))

(defn función-comp-15
  []
  (let [f (fn [xs] (+ 2 xs))
        g (fn [x] (* 4 x))
        z (comp f g)]
    (z 20)))

(defn función-comp-16
  []
  (let [f (fn [xs] (+ 2 xs))
        g (fn [x] (- 8 x))
        z (comp f g)]
    (z 11)))

(defn función-comp-17
  []
  (let [f (fn [xs] (- 3 xs))
        g (fn [x] (- 5 x))
        z (comp f g)]
    (z 7)))

(defn función-comp-18
  []
  (let [f (fn [xs] (* 1 xs))
        g (fn [x] (* 2 x))
        z (comp f g)]
    (z 10)))

(defn función-comp-19
  []
  (let [f (fn [xs] (/ xs 50))
        g (fn [x] (/ 3 x))
        h (comp f g)]
    (h 1000)))

(defn función-comp-20
  []
  (let [f (fn [xs] (- xs 4))
        g (fn [x] (+ x 4))
        z (comp f g)]
    (z 11)))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)

(defn función-complement-1
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 11)))

(defn función-complement-2
  []
  (let [f (fn [x] (empty? x))
        z (complement f)]
    (z [20 11 15 17])))

(defn función-complement-3
  []
  (let [f (fn [x] (decimal? x))
        z (complement f)]
    (z 3.14159)))

(defn función-complement-4
  []
  (let [f (fn [x] (double? x))
        z (complement f)]
    (z 314151914565.14159115465548798754)))

(defn función-complement-5
  []
  (let [f (fn [x] (zero? x))
        z (complement f)]
    (z 3.14159)))

(defn función-complement-6
  []
  (let [f (fn [x] (list? x))
        z (complement f)]
    (z '(3.14))))

(defn función-complement-7
  []
  (let [f (fn [x] (coll? x))
        z (complement f)]
    (z #(3.14))))

(defn función-complement-8
  []
  (let [f (fn [x] (coll? x))
        z (complement f)]
    (z '(3.14))))

(defn función-complement-9
  []
  (let [f (fn [x] (int? x))
        z (complement f)]
    (z '(3.14))))

(defn función-complement-10
  []
  (let [f (fn [x] (neg? x))
        z (complement f)]
    (z 3.1415)))

(defn función-complement-11
  []
  (let [f (fn [x] (sequential? x))
        z (complement f)]
    (z [1 20 3 4 5 10])))

(defn función-complement-12
  []
  (let [f (fn [x] (symbol? x))
        z (complement f)]
    (z "&")))

(defn función-complement-13
  []
  (let [f (fn [x] (pos-int? x))
        z (complement f)]
    (z 3.14159)))

(defn función-complement-14
  []
  (let [f (fn [x] (zero? x))
        z (complement f)]
    (z 2r00)))


(defn función-complement-15
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 1)))

(defn función-complement-16
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 2)))

(defn función-complement-17
  []
  (let [f (fn [x] (number? x))
        z (complement f)]
    (z "&")))

(defn función-complement-18
  []
  (let [f (fn [x] (number? x))
        z (complement f)]
    (z 3.14)))

(defn función-complement-19
  []
  (let [f (fn [x] (ratio? x))
        z (complement f)]
    (z 5/9)))


(defn función-complement-20
  []
  (let [f (fn [x] (vector? x))
        z (complement f)]
    (z [1 2 3 4])))


(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

(defn función-constantly-1
  []
  (let [x true
        z (constantly x)]
    (z 3.1415)))

(defn función-constantly-2
  []
  (let [x false
        z (constantly x)]
    (z true)))

(defn función-constantly-3
  []
  (let [x "Israel"
        z (constantly x)]
    (z "Avendaño")))

(defn función-constantly-4
  []
  (let [x [3 5 8 13 21]
        z (constantly x)]
    (z [1 2 3 4])))

(defn función-constantly-5
  []
  (let [x [\f \i \b \o \n \a \c \c \i]
        z (constantly x)]
    (z "fibonacci")))

(defn función-constantly-6
  []
  (let [x '(3 5 8 13)
        z (constantly x)]
    (z '(1 2 3))))

(defn función-constantly-7
  []
  (let [x '(+ 3 5)
        z (constantly x)]
    (z '(+ 1 3))))

(defn función-constantly-8
  []
  (let [x 'f
        z (constantly x)]
    (z 'i')))

(defn función-constantly-9
  []
  (let [x {'a 'b 'c 'd}
        z (constantly x)]
    (z {'a 'b})))

(defn función-constantly-10
  []
  (let [x #{10 20 30}
        z (constantly x)]
    (z 1 2 3)))

(defn función-constantly-11
  []
  (let [x (* 3 5)
        z (constantly x)]
    (z (* 3 2))))

(defn función-constantly-12
  []
  (let [x (/ 3 5)
        z (constantly x)]
    (z (* 3 2))))

(defn función-constantly-13
  []
  (let [x (rational? 13)
        z (constantly x)]
    (z 13)))

(defn función-constantly-14
  []
  (let [x "Oaxaca"
        z (constantly x)]
    (z "México")))

(defn función-constantly-15
  []
  (let [x (* 3 5)
        z (constantly x)]
    (z (/ 3 2))))

(defn función-constantly-16
  []
  (let [x #{1 2 3}
        z (constantly x)]
    (z 3 4 5)))

(defn función-constantly-17
  []
  (let [x (- 3 5)
        z (constantly x)]
    (z (+ 3 2))))

(defn función-constantly-18
  []
  (let [x (int? 'f)
        z (constantly x)]
    (z 'f)))

(defn función-constantly-19
  []
  (let [x (double? 3.14)
        z (constantly x)]
    (z 3.14)))

(defn función-constantly-20
  []
  (let [x (var? 2)
        z (constantly x)]
    (z 2)))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

(defn función-every-pred-1
  []
  (let [f (fn [x] (pos? x))
        z (every-pred f)]
    (z 13)))


(defn función-every-pred-2
  []
  (let [f (fn [x] (number? x))
        z (every-pred f)]
    (z "Israel")))

(defn función-every-pred-3
  []
  (let [f (fn [x] (number? x))
        z (every-pred f)]
    (z 3.1415)))

(defn función-every-pred-4
  []
  (let [f (fn [x] (map? x))
        z (every-pred f)]
    (z (conj {:a 1 :b 2} [:c 3] [:d 4]))))

(defn función-every-pred-5
  []
  (let [f (fn [x] (empty? x))
        z (every-pred f)]
    (z [])))

(defn función-every-pred-6
  []
  (let [f (fn [x] (empty? x))
        z (every-pred f)]
    (z "Avendaño")))

(defn función-every-pred-7
  []
  (let [f (fn [x] (string? x))
        z (every-pred f)]
    (z "Oaxaca")))

(defn función-every-pred-8
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (double? x))
        z (every-pred f g)]
    (z 3.14)))

(defn función-every-pred-9
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (double? x))
        z (every-pred f g)]
    (z 3.14)))

(defn función-every-pred-10
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (int? x))
        h (fn [x] (pos? x))
        i (fn [x] (even? x))
        z (every-pred f g h i)]
    (z 2)))

(defn función-every-pred-11
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (associative? xs))
        h (fn [xs] (sequential? xs))
        z (every-pred f g h)]
    (z [3 5 8 13 21])))

(defn función-every-pred-12
  []
  (let [f (fn [x] (double? x))
        g (fn [x] (even? x))
        h (fn [x] (pos? x))
        i (fn [x] (ratio? x))
        z (every-pred f g h i)]
    (z 2)))

(defn función-every-pred-13
  []
  (let [f (fn [x] (ratio? x))
        g (fn [x] (rational? x))
        h (fn [x] (pos? x))
        i (fn [x] (int? x))
        z (every-pred f g h i)]
    (z 2)))

(defn función-every-pred-14
  []
  (let [f (fn [xs] (map even? xs))
        g (fn [xs] (map boolean? xs))
        h (fn [xs] (map true? xs))
        z (every-pred f g h)]
    (z [3 5 8 15 21 36])))


(defn función-every-pred-15
  []
  (let [f (fn [x] (+ 10 x))
        g (fn [x] (- 10 x))
        z (every-pred f g)]
    (z 5 8)))

(defn función-every-pred-16
  []
  (let [f (fn [x] (some? x))
        g (fn [x] (true? x))
        z (every-pred f g)]
    (z false)))

(defn función-every-pred-17
  []
  (let [f (fn [x] (some? x))
        g (fn [x] (true? x))
        z (every-pred f g)]
    (z true)))

(defn función-every-pred-18
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (int? x))
        h (fn [x] (pos? x))
        z (every-pred f g h)]
    (z 8)))

(defn función-every-pred-19
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (odd? x))
        z (every-pred f g)]
    (z 1 2 3 )))

(defn función-every-pred-20
  []
  (let [f (fn [x] (coll? x))
        g (fn [x] (vector? x))
        z (every-pred f g)]
    (z [3 5 8 13])))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

(defn función-fnil-1
  []
  (let [f (fn [x] (dec x))
        z (fnil f nil)]
    (z 3)))

(defn función-fnil-2
  []
  (let [f (fn [x] (dec x))
        z (fnil f nil)]
    (z 0)))

(defn función-fnil-3
  []
  (let [f (fn [x] (inc x))
        z (fnil f nil)]
    (z 3)))
(función-fnil-3)

(defn función-fnil-4
  []
  (let [f (fn [x y] (if (- x y) (+ x y) (* x y)))
        z (fnil f nil nil)]
    (z 5 8)))

(defn función-fnil-5
  []
  (let [f (fn [xs ys] (conj [] xs ys))
        z (fnil f '(3 5 8))]
    (z nil '(13 21 34))))

(defn función-fnil-6
  []
  (let [f (fn [x y] (if (+ x y) (- x y)))
        z (fnil f nil nil)]
    (z 5 8)))

(defn función-fnil-7
  []
  (let [f (fn [x y] (if (+ x y) (- x y)))
        z (fnil f nil)]
    (z 5 3)))

(defn función-fnil-8
  []
  (let [f (fn [x] (keyword? x))
        z (fnil f nil)]
    (z 3.14)))

(defn función-fnil-9
  []
  (let [f (fn [x] (keyword? x))
        z (fnil f nil)]
    (z :z)))

(defn función-fnil-10
  []
  (let [f (fn [x y] (* x y))
        z (fnil f nil nil)]
    (z 3 2)))

(defn función-fnil-11
  []
  (let [f (fn [xs ys] (count (into xs ys)))
        z (fnil f nil nil)]
    (z [0 1 2 3] [10 11 12 13])))
(función-fnil-11)

(defn función-fnil-12
  []
  (let [f (fn [xs] (into [] (remove pos? xs)))
        z (fnil f nil)]
    (z [ 1 1 2 3 5 8 13 21])))


(defn función-fnil-13
  []
  (let [f (fn [s] (str "Israel " s))
        z (fnil f nil)]
    (z "Avendaño")))

(defn función-fnil-14
  []
  (let [f (fn [xs] (double? xs))
        z (fnil f nil)]
    (z 5.4)))

(defn función-fnil-15
  []
  (let [f (fn [xs] (double? xs))
        z (fnil f nil)]
    (z 5)))

(defn función-fnil-16
  []
  (let [f (fn [x] (map char x))
        z (fnil f nil)]
    (z (range 64 81))))


(defn función-fnil-17
  []
  (let [f (fn [x] (map char x))
        z (fnil f nil)]
    (z (range  0 15 ))))

(defn función-fnil-18
  []
  (let [f (fn [xs x y] (conj xs x y))
        z (fnil f nil nil nil)]
    (z [1 2 3] 40 50 )))

(defn función-fnil-19
  []
  (let [f (fn [xs] (integer? xs))
        z (fnil f nil)]
    (z "Israel")))

(defn función-fnil-20
  []
  (let [f (fn [xs] (odd? xs))
        z (fnil f nil)]
    (z 3)))


(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)


(defn función-juxt-1
  []
  (let [f (fn [x] (count x))
        z (juxt f)]
    (z "Israel")))

(defn función-juxt-2
  []
  (let [f (fn [xs] (first xs))
        g (fn [x] (count x))
        z (juxt f g)]
    (z "Israel Avendaño")))

(defn función-juxt-3
  []
  (let [f (fn [xs] (conj xs))
        g (fn [x] (str x))
        z (juxt g f)]
    (z [1 1 2 3 5 8 13 21])))

(defn función-juxt-4
  []
  (let [f (fn [xs] (count xs))
        g (fn [x] (last x))
        z (juxt g f)]
    (z [:a 3 :b 5 :c 8 :d 13])))

(defn función-juxt-5
  []
  (let [f (fn [xs] (list? xs))
        z (juxt f)]
    (z #(1 1 2 3 5 8 13))))

(defn función-juxt-6
  []
  (let [f (fn [xs] (list? xs))
        z (juxt f)]
    (z '(1 1 2 3 5 8 13))))

(defn función-juxt-7
  []
  (let [f (fn [x] (sort x))
        g (fn [x] (conj [\a \b] x))
        z (juxt f g)]
    (z "Israel Avendaño")))

(defn función-juxt-8
  []
  (let [f (fn [xs] (empty? xs))
        g (fn [xs] (vector? xs))
        z (juxt f g)]
    (z [])))

(defn función-juxt-9
  []
  (let [f (fn [xs] (empty? xs))
        g (fn [xs] (vector? xs))
        z (juxt f g)]
    (z [1 1 3 5])))

(defn función-juxt-10
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (dec x))
        h (fn [x] (* x x))
        z (juxt g h f)]
    (z 3)))

(defn función-juxt-11
  []
  (let [f (fn [x] (take 3 x))
        g (fn [x] (reverse x))
        z (juxt g f)]
    (z "1234567890")))

(defn función-juxt-12
  []
  (let [f (fn [x] (take 3 x))
        g (fn [x] (str x))
        z (juxt g f)]
    (z "0987654321")))

(defn función-juxt-13
  []
  (let [f (fn [x] (+ x x))
        g (fn [x] (/ x x))
        h (fn [x] (- x x))
        z (juxt f g h)]
    (z 3)))

(defn función-juxt-14
  []
  (let [f (fn [x] (- x 3.1415))
        g (fn [x] (pos? x))
        h (fn [xs] (range xs))
        i (fn [x] (even? x))
        z (juxt f h g i)]
    (z 3)))

(defn función-juxt-15
  []
  (let [f (fn [x] (count x))
        g (fn [xs] (filter char? xs))
        h (fn [x] (vector x))
        z (juxt f g h)]
    (z #{10 "Aa" 11 "Bb" 12 "Cc" 13 "Dd"})))

(defn función-juxt-16
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (double? x))
        z (juxt f g)]
    (z -3.14)))

(defn función-juxt-17
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (int? x))
        i (fn [x] (even? x))
        j (fn [x] (* x x))
        z (juxt f g i j)]
    (z 3)))

(defn función-juxt-18
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [x] (list? x))
        h (fn [x] (map? x))
        z (juxt f g h)]
    (z '("A" "B" "C" "D"))))

(defn función-juxt-19
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [x] (list? x))
        h (fn [x] (map? x))
        i (fn [x] (some? x))
        z (juxt f g h i)]
    (z #("A" "B" "C" "D"))))

(defn función-juxt-20
  []
  (let [f (fn [xs] (count xs))
        g (fn [x] (last x))
        z (juxt f g)]
    (z [1 1 2 3 5 8 13 21])))


(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)


(defn función-partial-1
  []
  (let [f (fn [x] (* 3 x))
        z (partial f)]
    (z 5)))

(defn función-partial-2
  []
  (let [f (fn [k l m n o] (hash-set k l m n o))
        z (partial f)]
    (z 1 2 3 5 8)))

(defn función-partial-3
  []
  (let [f (fn [x] (/ 3 x))
        z (partial f)]
    (z 5)))

(defn función-partial-4
  []
  (let [f (fn [x] (dec x))
        z (partial f)]
    (z 8)))

(defn función-partial-5
  []
  (let [f (fn [x] (inc x))
        z (partial f)]
    (z 8)))

(defn función-partial-6
  []
  (let [f (fn [k l m n o] (list k l m n o))
        z (partial f)]
    (z 1 2 3 5 8)))

(defn función-partial-7
  []
  (let [f (fn [k l m n o] (vector k l m n o))
        z (partial f)]
    (z 1 2 3 5 8)))

(defn función-partial-8
  []
  (let [f (fn [xs] (map char xs))
        z (partial f)]
    (z (range 65 81))))

(defn función-partial-9
  []
  (let [f (fn [k l m] (conj [] k l m))
        z (partial f [1 2 3])]
    (z [1 1 2] [3 5 8])))

(defn función-partial-10
  []
  (let [f (fn [k l] (into k l))
        z (partial f [])]
    (z [3 5])))

(defn función-partial-11
  []
  (let [f (fn [x] (sequential? x))
        z (partial f)]
    (z "Israel Avendaño")))

(defn función-partial-12
  []
  (let [f (fn [x] (sequential? x))
        z (partial f)]
    (z [1 2 3 4])))

(defn función-partial-13
  []
  (let [f (fn [x] (filter number? x))
        z (partial f)]
    (z "Israel")))

(defn función-partial-14
  []
  (let [f (fn [x] (filter string? x))
        z (partial f)]
    (z "Israel")))

(defn función-partial-15
  []
  (let [f (fn [x] (filter char? x))
        z (partial f)]
    (z "Israel")))

(defn función-partial-16
  []
  (let [f (fn [k l m] (concat k l m))
        z (partial f #{1 2 3})]
    (z [5 8 13] '(21 34 55))))

(defn función-partial-17
  []
  (let [f (fn [x] (filter pos? x))
        z (partial f)]
    (z [-3 -2 -1 0 1 2 3])))

(defn función-partial-18
  []
  (let [f (fn [x] (remove pos? x))
        z (partial f)]
    (z [-3 -2 -1 0 1 2 3])))

(defn función-partial-19
  []
  (let [f (fn [x] (remove even? x))
        z (partial f)]
    (z (range -10 10))))

(defn función-partial-20
  []
  (let [f (fn [x] (take-while string? x))
        z (partial f)]
    (z ["Fibonacci" 1 1 2 3 5])))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)


(defn función-some-fn-1
  []
  (let [f (fn [x] (even? x))
        z (some-fn f)]
    (z 3)))

(defn función-some-fn-2
  []
  (let [f (fn [x] (number? x))
        z (some-fn f)]
    (z 3)))

(defn función-some-fn-3
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (even? x))
        h (fn [x] (neg? x))
        z (some-fn f g h)]
    (z 8)))

(defn función-some-fn-4
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (even? x))
        h (fn [x] (double x))
        i (fn [x] (pos? x))
        z (some-fn f g h i)]
    (z 8/8)))

(defn función-some-fn-5
  []
  (let [f (fn [x] (filter pos? x))
        z (some-fn f)]
    (z #{-3 -2 -1 0 1 2 3})))

(defn función-some-fn-6
  []
  (let [f (fn [x] (filter number? x))
        z (some-fn f)]
    (z #{-3 -2 -1 0 1 2 3 'a 'b})))


(defn función-some-fn-7
  []
  (let [f (fn [x] (filter char? x))
        z (some-fn f)]
    (z #{\a \b 4 5})))

(defn función-some-fn-8
  []
  (let [f (fn [x] (filter string? x))
        z (some-fn f)]
    (z #{"Israel" 1 2 3})))

(defn función-some-fn-9
  []
  (let [f (fn [x] (filter even? x))
        z (some-fn f)]
    (z [1 2 3 4 5 6 7 8])))


(defn función-some-fn-10
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (associative? x))
        h (fn [x] (sequential? x))
        z (some-fn f g h)]
    (z [1 1 3 5 8 13 21])))


(defn función-some-fn-11
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (pos? x))
        h (fn [x] (int? x))
        h (fn [x] (even? x))
        z (some-fn f g h)]
    (z 8)))

(defn función-some-fn-12
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (vector? x))
        z (some-fn f g)]
    (z [])))

(defn función-some-fn-13
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (vector? x))
        i (fn [x] (string? x))
        z (some-fn f g i)]
    (z [1 2 3])))

(defn función-some-fn-14
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (char? x))
        g (fn [x] (empty? x))
        z (some-fn f g)]
    (z "Oaxaca Mexico")))

(defn función-some-fn-15
  []
  (let [f (fn [x] (list? x))
        g (fn [x] (number? x))
        z (some-fn f g)]
    (z '(1 1 2 3 5 ))))

(defn función-some-fn-16
  []
  (let [f (fn [x] (keyword? x))
        g (fn [x] (char? x))
        z (some-fn g f)]
    (z \I)))

(defn función-some-fn-17
  []
  (let [f (fn [x] (count x))
        g (fn [xs] (filter char? xs))
        h (fn [x] (vector x))
        z (some-fn f g h)]
    (z #{10 "Aa" 11 "Bb" 12 "Cc" 13 "Dd"})))

(defn función-some-fn-18
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (dec x))
        h (fn [x] (* x x))
        z (some-fn g h f)]
    (z 3)))


(defn función-some-fn-19
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (int? x))
        i (fn [x] (even? x))
        j (fn [x] (* x x))
        z (some-fn f g i j)]
    (z 3)))

(defn función-some-fn-20
  []
  (let [f (fn [x] (+ x x))
        g (fn [x] (/ x x))
        h (fn [x] (- x x))
        z (some-fn f g h)]
    (z 3)))


(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)

;Israel Avendaño
;Eskape---
;
